#include "bmp.h"
#include "pixel.h"

#define BMP_CODE 0x4D42
#define DEFAULT_BISIZE 40
#define DEFAULT_BIPLANES  1
#define BMP_BIT_COUNT 24
#define ALIGNMENT_BYTE 4
#define DEFAULT_BMP_RESERVED 0
#define DEFAULT_BMP_COMPRESSION 0
#define DEFAULT_BMP_CLR_IMPORTANT 0
#define DEFAULT_BMP_CLR_USED 0
#define DEFAULT_BMP_X_PELS_PER_METER 0
#define DEFAULT_BMP_Y_PELS_PER_METER 0

uint32_t calc_padding(const uint32_t width) {
    return (ALIGNMENT_BYTE - ((width * sizeof(struct pixel)) % ALIGNMENT_BYTE)) % ALIGNMENT_BYTE;
}

struct bmp_header new_bmp_header(const struct image *img) {
    struct bmp_header header = {0};

    uint32_t row_padding = calc_padding(img->width);
    header.bfType = BMP_CODE;
    header.bfileSize = sizeof(struct bmp_header) + (img->height) * (img->width * sizeof(struct pixel) + row_padding);
    header.bfReserved = DEFAULT_BMP_RESERVED;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = DEFAULT_BISIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = DEFAULT_BIPLANES;
    header.biBitCount = BMP_BIT_COUNT;
    header.biCompression = DEFAULT_BMP_COMPRESSION;
    header.biSizeImage = img->height * img->width * sizeof(struct pixel);
    header.biXPelsPerMeter = DEFAULT_BMP_X_PELS_PER_METER;
    header.biYPelsPerMeter = DEFAULT_BMP_Y_PELS_PER_METER;
    header.biClrUsed = DEFAULT_BMP_CLR_USED;
    header.biClrImportant = DEFAULT_BMP_CLR_IMPORTANT;

    return header;
}


