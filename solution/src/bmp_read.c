#include "bmp_read.h"
#include "pixel.h"
#include <stdio.h>

#define BMP_CODE 0x4D42

enum parse_status parse_bmp_header(FILE *file_stream, struct bmp_header *bmp_header_struct) {
    const size_t signature_size = sizeof(uint16_t);
    const size_t header_data_size = sizeof(struct bmp_header) - signature_size;

    const size_t signature_read = fread(&bmp_header_struct->bfType, signature_size, 1, file_stream);

    const size_t remaining_header_read = fread((uint8_t *) bmp_header_struct + signature_size, header_data_size, 1,
                                               file_stream);

    if (remaining_header_read != 1) return PARSE_INVALID_HEADER;

    if (bmp_header_struct->bfType != BMP_CODE || signature_read != 1) return PARSE_INVALID_SIGNATURE;

    return PARSE_OK;
}

enum read_status read_bmp_pixel_data(FILE *input_stream, struct bmp_header bmpHeader, struct image *output_image) {

    struct image newImage=new_image(bmpHeader.biWidth, bmpHeader.biHeight);
    if(newImage.width==0 && newImage.height==0) {
        return READ_MEMORY_ERROR;
    }else {
        *output_image=newImage;
    }
    const uint32_t padding = calc_padding(output_image->width);

    for (uint32_t row = 0; row < output_image->height; row++) {
        size_t pixels_to_read = output_image->width;
        size_t pixels_read = fread(&output_image->data[row * output_image->width], sizeof(struct pixel), pixels_to_read,
                                   input_stream);
        if (pixels_read != pixels_to_read) {
            if (feof(input_stream)) {
                return READ_EOF_BEFORE_PIXELS;
            } else if (ferror(input_stream)) {
                return READ_STREAM_ERROR;
            } else {
                return READ_INVALID_BITS;
            }
        }
        if (fseek(input_stream, (long) padding, 1) != 0) return ERROR_WITH_SKIP_PADDING;
    }
    return READ_OK;
}


enum read_status read_image_from_bmp(FILE *bmp_file, struct image *output_image) {
    if (!bmp_file || !output_image) {
        return READ_INVALID_ARGUMENTS;
    }
    struct bmp_header bmp_header_data = {0};
    enum parse_status read_header_status = parse_bmp_header(bmp_file, &bmp_header_data);
    if (read_header_status != PARSE_OK) return READ_STATUS_PARSE_ERROR;
    return read_bmp_pixel_data(bmp_file, bmp_header_data, output_image);
}


