#include "angle_operations.h"
#include "file_utils.h"
#include <stdio.h>
#include <unistd.h>

enum validate_arguments_status validate_input_count(const uint16_t argc) {
    if (argc != EXPECTED_ARGUMENTS) {
        perror("Invalid number of arguments. Expected format: <source-image> <transformed-image> <angle>");
        return EXPECTED_ARGUMENTS_ERROR;
    }
    return EXPECTED_ARGUMENTS_SUCCESSFUL;
}

enum validate_angle_status normalize_angle(const int32_t angle) {
    const int valid_angles[] = VALID_ANGLES;
    int num_valid_angles = sizeof(valid_angles) / sizeof(valid_angles[0]);

    for (int i = 0; i < num_valid_angles; ++i) {
        if (angle == valid_angles[i]) {
            return VALIDATE_SUCCESSFUL;
        }
    }
    perror("Invalid angle. Please use 0, -90, 90, -180, 180, -270, or 270.");
    return INVALID_ANGLE_VALUE;
}

enum file_access_status validate_read_access(const char *source_image) {
    if (access(source_image, R_OK) == ACCESS_ERROR_CODE) {
        perror("Failed to open or process the file");
        return FILE_ACCESS_ERROR;
    }
    return FILE_ACCESS_SUCCESSFUL;
}

enum file_access_status validate_write_access(const char *transformed_image) {
    if (access(transformed_image, W_OK) == ACCESS_ERROR_CODE) {
        perror("Failed to open or process the file");
        return FILE_ACCESS_ERROR;
    }
    return FILE_ACCESS_SUCCESSFUL;
}

enum validate_angle_status validate_angle(const char *endptr, int16_t angle, char *argv) {
    //Можно добавить ограничения на angle. Не думаю, что значения >|10000| должны считаться валидными
    //Ограничил в пределах 5000 (Условно)
    if (*endptr != '\0' || angle > VALIDATE_MAX_VALUE || angle < VALIDATE_MIN_VALUE) {
        fprintf(stderr, "Invalid angle value: %s\n", argv);
        return INVALID_ANGLE_VALUE;
    }
    return VALIDATE_SUCCESSFUL;
}

// Функция для обработки ошибок при открытии файла
void handle_file_error(FILE *file, const char *errorMessage, struct image *imageData) {
    perror(errorMessage);
    if (file != NULL) {
        fclose(file);
    }
    if (imageData->data != NULL) {
        cleanup_image(imageData);
    }
}

