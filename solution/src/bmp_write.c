#include "bmp.h"
#include "bmp_write.h"
#include "pixel.h"
#include <stdio.h>

//Записывает заголовок BMP в файл.
enum write_status write_bmp_header(FILE *output, const struct bmp_header *bmpHeader) {
    return (fwrite(bmpHeader, sizeof(struct bmp_header), 1, output) == 1) ? WRITE_OK : WRITE_HEADER_ERROR;
}

// Записывает данные изображения в файл с учетом выравнивания строк.
enum write_status write_bmp_image_data(FILE *output, const struct image *image) {
    uint32_t padding = calc_padding(image->width);

    for (uint32_t row = 0; row < image->height; row++) {
        uint32_t paddingByte = 0;

        size_t pixels_written = fwrite(row * image->width + image->data, sizeof(struct pixel), image->width, output);
        size_t padding_written = fwrite(&paddingByte, sizeof(uint8_t), padding, output);
        if (pixels_written != image->width) return WRITE_IMAGE_DATA_ERROR;
        if (padding_written != padding) return WRITE_PADDING_ERROR;
    }
    return WRITE_OK;
}

enum write_status write_image_to_bmp(FILE *output, const struct image *image) {
    struct bmp_header bmp_header = new_bmp_header(image);

    enum write_status header_write_status = write_bmp_header(output, &bmp_header);
    enum write_status image_data_write_status = write_bmp_image_data(output, image);

    return (header_write_status != WRITE_OK || image_data_write_status != WRITE_OK) ? WRITE_ERROR : WRITE_OK;
}


