#include "image.h"
#include <stdio.h>
#include <stdlib.h>

void cleanup_image_memory(struct image *image) {
    if (image->data != NULL) {
        free(image->data);
        image->data = NULL;
    }
}

void cleanup_image(struct image *image) {
    if (image->data != NULL) {
        free(image->data);
    }
    free(image);
}

struct image new_image(const uint32_t width, const uint32_t height) {
    struct image img = {
            .width = width,
            .height = height,
            .data = malloc(sizeof(struct pixel) * width * height)
    };
    if (img.data == NULL) {
        // Обработка ошибки при выделении памяти
        perror("Error: Unable to allocate memory for image data\\n");
        img.width = img.height = 0;
    }
    return img;
}


