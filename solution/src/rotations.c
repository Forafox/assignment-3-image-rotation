#include "image.h"

#define FULL_ROTATION_DEGREES 360
#define DEGREES_PER_QUARTER 90
#define ROTATIONS_PER_QUARTER 4

struct image rotate_image_90_degrees(const struct image source) {
    struct image newImage = new_image(source.height, source.width);
    if(newImage.height==0 && newImage.width==0){
        return newImage;
    }
    // Проходим через каждый пиксель исходного изображения
    for (uint32_t source_row = 0; source_row < source.height; source_row++) {
        for (uint32_t source_col = 0; source_col < source.width; source_col++) {
            // Инвертируем координаты для поворота изображения на 90 градусов
            uint32_t rotated_row = source_col;
            uint32_t rotated_col = newImage.width - 1 - source_row;
            // Копируем значение пикселя из исходного изображения в повернутое изображение
            *(newImage.data + rotated_row * newImage.width + rotated_col) =
                    *(source.data + source_row * source.width + source_col);
        }
    }
    return newImage;
}

struct image rotate_image(uint16_t angle, struct image img) {
    // Преобразуем угол в диапазон [0, 360)
    angle = ((angle + FULL_ROTATION_DEGREES) % FULL_ROTATION_DEGREES);
    //и вычисляем количество поворотов по часовой стрелке на 90 градусов
    uint16_t rotations = (ROTATIONS_PER_QUARTER - angle / DEGREES_PER_QUARTER) % ROTATIONS_PER_QUARTER;
    while (rotations-- > 0) {
        struct image newImage = rotate_image_90_degrees(img);
        cleanup_image_memory(&img);
        img = newImage;
    }
    return img;
}
