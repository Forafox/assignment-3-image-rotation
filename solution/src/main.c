#include "angle_operations.h"
#include "bmp.h"
#include "bmp_read.h"
#include "bmp_write.h"
#include "file_utils.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    if (validate_input_count(argc) != EXPECTED_ARGUMENTS_SUCCESSFUL) {
        return EXIT_FAILURE;
    }

    const char *image = argv[1];
    const char *newImage = argv[2];
    char *endptr;  // указатель на первый символ
    int16_t angle = (int16_t) strtol(argv[3], &endptr, 10);
    if (validate_angle(endptr, angle, argv[3]) != VALIDATE_SUCCESSFUL) {
        return EXIT_FAILURE;
    }
    // Проверка угла
    if (normalize_angle(angle) != VALIDATE_SUCCESSFUL) {
        return EXIT_FAILURE;
    }
    // Проверка доступа к чтению
    if (validate_read_access(image) != FILE_ACCESS_SUCCESSFUL) {
        return EXIT_FAILURE;
    }

    // Открытие и чтение входного файла
    FILE *in = fopen(image, "rb");
    if (in == NULL) {
        handle_file_error(in, "Failed to open source file", &(struct image) {0});
        return EXIT_FAILURE;
    }

    struct image loadedImage = {0};
    if (read_image_from_bmp(in, &loadedImage) != READ_OK) {
        handle_file_error(in, "Error reading input file", &loadedImage);
        return EXIT_FAILURE;
    }
    fclose(in);

    // Проверка доступа к записи
    if (validate_write_access(image) != FILE_ACCESS_SUCCESSFUL) {
        return EXIT_FAILURE;
    }
    //Поворачиваем изображение
    loadedImage = rotate_image(angle, loadedImage);
    if(loadedImage.height==0 && loadedImage.width==0){
        return EXIT_FAILURE;
    }

    // Открытие и запись в выходной файл
    FILE *out = fopen(newImage, "wb");
    if (out == NULL) {
        handle_file_error(out, "Failed to open transformed file", &loadedImage);
        return EXIT_FAILURE;
    }

    if (write_image_to_bmp(out, &loadedImage) != WRITE_OK) {
        handle_file_error(out, "Error writing output file", &loadedImage);
        return EXIT_FAILURE;
    }

    fclose(out);
    // Освобождение ресурсов при успешном выполнении
    cleanup_image_memory(&loadedImage);
    return EXIT_SUCCESS;
}
