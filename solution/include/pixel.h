#ifndef PIXEL_H
#define PIXEL_H

#include <stdint.h>

/* структура размером 3 байта*/
struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

#endif //PIXEL_H
