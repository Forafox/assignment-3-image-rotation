#ifndef BMP_WRITE_H
#define BMP_WRITE_H

#include "bmp.h"
#include "image.h"
#include <stdio.h>

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_HEADER_ERROR,
    WRITE_IMAGE_DATA_ERROR,
    WRITE_PADDING_ERROR
};

enum write_status write_image_to_bmp(FILE *output, const struct image *image);

enum write_status write_bmp_image_data(FILE *output, const struct image *image);

enum write_status write_bmp_header(FILE *output, const struct bmp_header *bmpHeader);

#endif //BMP_WRITE_H
