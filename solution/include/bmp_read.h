#ifndef BMP_READ_H
#define BMP_READ_H

#include "bmp.h"
#include "image.h"
#include <stdio.h>

enum read_status {
    READ_OK = 0,
    READ_STATUS_PARSE_ERROR,
    READ_INVALID_BITS,
    ERROR_WITH_SKIP_PADDING,
    READ_EOF_BEFORE_PIXELS,
    READ_STREAM_ERROR,
    READ_INVALID_ARGUMENTS,
    READ_MEMORY_ERROR
};

enum parse_status {
    PARSE_OK,
    PARSE_INVALID_HEADER,
    PARSE_INVALID_SIGNATURE
};

enum read_status read_image_from_bmp(FILE *bmp_file, struct image *output_image);

enum parse_status parse_bmp_header(FILE *file_stream, struct bmp_header *bmp_header_struct);

enum read_status read_bmp_pixel_data(FILE *input_stream, struct bmp_header bmpHeader, struct image *output_image);

#endif //BMP_READ_H
