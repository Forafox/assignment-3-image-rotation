#include <pixel.h>
#include <stdint.h>

#ifndef IMAGE_H
#define IMAGE_H
/*Внутренее представление картинки, очищенное от деталей формата,
 * и функции для работы с ним:
 * создание, деинициализация и т.д.
 */
struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image new_image(uint32_t width, uint32_t height);

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate_image_90_degrees(struct image source);

/* Поворачивает изображение на необходимое кол-во градусов */
struct image rotate_image(uint16_t angle, struct image img_struct);

void cleanup_image_memory(struct image *image);

void cleanup_image(struct image *image);

#endif//IMAGE_H
