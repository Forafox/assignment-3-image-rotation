#include "image.h"
#include <stdint.h>
#include <stdio.h>

#ifndef BMP_H
#define BMP_H
struct __attribute__((packed)) bmp_header {
    uint16_t bfType;        // тип файла
    uint32_t bfileSize;     // размер файла в байтах
    uint32_t bfReserved;    // резерв
    uint32_t bOffBits;      // Смещение (кол-во байт от начала файла до начала данных)
    uint32_t biSize;        // размер структуры bmp_header в байтах
    uint32_t biWidth;       // Ширина изображения в пикселях
    uint32_t biHeight;      // Высота изображения в пикселях
    uint16_t biPlanes;      // Кол-во цветовых плоскостей
    uint16_t biBitCount;    // кол-во бит на пиксель
    uint32_t biCompression; // метод сжатия изображения
    uint32_t biSizeImage;   // размер изображения
    uint32_t biXPelsPerMeter;   // разрешение по горизонтали в пикселях на метр
    uint32_t biYPelsPerMeter;   // разрешение по вертикали в пикселях на метр
    uint32_t biClrUsed;         // кол-во используемых цветов
    uint32_t biClrImportant;    // кол-во "важных" цветов
};

uint32_t calc_padding(uint32_t width);

struct bmp_header new_bmp_header(struct image const *img);

#endif //BMP_H
