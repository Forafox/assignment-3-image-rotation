#include <image.h>
#include <stdint.h>
#include <stdio.h>

#ifndef FILE_UTILS_H
#define FILE_UTILS_H

#define EXPECTED_ARGUMENTS 4
#define ACCESS_ERROR_CODE (-1)

enum file_access_status {
    FILE_ACCESS_SUCCESSFUL,
    FILE_ACCESS_ERROR
};
enum validate_arguments_status {
    EXPECTED_ARGUMENTS_SUCCESSFUL,
    EXPECTED_ARGUMENTS_ERROR
};


enum validate_arguments_status validate_input_count(const uint16_t argc);

enum file_access_status validate_read_access(const char *source_image);

enum file_access_status validate_write_access(const char *transformed_image);

void handle_file_error(FILE *file, const char *errorMessage, struct image *imageData);

#endif // FILE_UTILS_H
