#ifndef ANGLE_OPERATIONS_H
#define ANGLE_OPERATIONS_H

#include <stdint.h>

#define VALID_ANGLES {0, -90, 90, -180, 180, -270, 270}
#define VALIDATE_MIN_VALUE (-5000)
#define VALIDATE_MAX_VALUE 5000

enum validate_angle_status {
    VALIDATE_SUCCESSFUL,
    INVALID_ANGLE_VALUE
};

enum validate_angle_status validate_angle(const char *endptr, int16_t angle, char *argv);

enum validate_angle_status normalize_angle(const int32_t angle);

#endif //ANGLE_OPERATIONS_H
